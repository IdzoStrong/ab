package com.idzodev.testing.ab.common.firebase

import com.idzodev.testing.ab.payments.NewPaymentResultViewFactory
import com.idzodev.testing.ab.payments.OldPaymentResultViewFactory

/**
 * Facade for Firebase remote config
 */
interface FirebaseRemoteConfig {
    fun getString(key: String): String
}

/**
 * Firebase remote config simulator.
 */
class FirebaseRemoteConfigEmulator: FirebaseRemoteConfig {
    override fun getString(key: String): String {
        return NewPaymentResultViewFactory.NAME
    }
}
