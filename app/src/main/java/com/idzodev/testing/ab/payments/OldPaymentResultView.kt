package com.idzodev.testing.ab.payments

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.idzodev.testing.ab.R
import com.idzodev.testing.ab.common.base.AbView
import com.idzodev.testing.ab.common.base.AbViewFactory
import kotlinx.android.synthetic.main.result_view.*

/**
 * Factory for creating old payment result view
 */
class OldPaymentResultViewFactory: AbViewFactory {
    override val featureValue: String = NAME

    override fun create(parent: ViewGroup): AbView {
        return OldPaymentsResultView(LayoutInflater.from(parent.context).inflate(R.layout.result_view, parent, false))
    }

    companion object {
        const val NAME = "DefltPaymentsResultView"
    }
}

/**
 * Old payment result view
 */
class OldPaymentsResultView(
    override val containerView: View
): AbView {
    val vm get() = ViewModelProviders.of(fragment!!).get(OldPaymentViewModel::class.java)
    val vText get() = result_view_text

    override fun observeView(target: LifecycleOwner) {
        vm.title.observe(target, Observer {
            vText.text = it
        })
    }
}

/**
 * Old view model
 */
class OldPaymentViewModel: ViewModel(){

    val title = MutableLiveData<String>()

    init {
        title.value = "Old payment result"
    }
}





