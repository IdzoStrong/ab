package com.idzodev.testing.ab.payments

import android.arch.lifecycle.LifecycleOwner
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.idzodev.testing.ab.R
import com.idzodev.testing.ab.common.base.AbView
import com.idzodev.testing.ab.common.base.AbViewFactory
import kotlinx.android.synthetic.main.new_result_view.*

/**
 * Factory for creating new payment result view
 */
class NewPaymentResultViewFactory: AbViewFactory {
    override val featureValue: String = NAME

    override fun create(parent: ViewGroup): AbView {
        return NewPaymentsResultView(LayoutInflater.from(parent.context).inflate(R.layout.new_result_view, parent, false))
    }

    companion object {
        const val NAME = "NewPaymentsResultView"
    }
}

/**
 * Simple view without any logic
 */
class NewPaymentsResultView(
    override val containerView: View
): AbView {
    val vImage get() = image

    override fun observeView(target: LifecycleOwner) {
        vImage.setImageResource(R.mipmap.ic_launcher)
    }

}
