package com.idzodev.testing.ab.common.base

import android.arch.lifecycle.LifecycleOwner
import android.content.Context
import android.view.ViewGroup
import kotlinx.android.extensions.LayoutContainer

/**
 * Factory that create [AbView] for feature key that come from remote
 */
interface AbViewFactory {
    val featureValue: String

    fun create(parent: ViewGroup): AbView
}

/**
 * View wrapper
 */
interface AbView: LayoutContainer, StrongViewExt {

    fun observeView(target: LifecycleOwner){}

    override fun getContext(): Context {
        return containerView!!.context
    }
}