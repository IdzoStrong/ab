package com.idzodev.testing.ab.common.base

import android.content.Context
import android.content.ContextWrapper
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import com.idzodev.testing.ab.R

/**
 * Simple ext for customs view
 */
interface StrongViewExt {
    fun getContext(): Context

    /**
     * Search activity in view hierarchy
     */
    val activity: FragmentActivity? get() {
        var context = getContext()
        while (context is ContextWrapper) {
            if (context is FragmentActivity) {
                return context
            }
            context = context.baseContext
        }
        return null
    }

    /**
     * Get current fragment from activity
     */
    val fragment: Fragment? get(){
        return activity?.let {
            it.supportFragmentManager.findFragmentById(R.id.fragment_container)
        }
    }
}
