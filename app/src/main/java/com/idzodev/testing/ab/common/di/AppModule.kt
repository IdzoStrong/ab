package com.idzodev.testing.ab.common.di

import android.content.Context
import com.idzodev.testing.ab.common.firebase.FirebaseRemoteConfig
import com.idzodev.testing.ab.common.firebase.FirebaseRemoteConfigEmulator
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(
    val context: Context
) {

    @Singleton
    @Provides
    fun context(): Context = context

    @Singleton
    @Provides
    fun frc(): FirebaseRemoteConfig = FirebaseRemoteConfigEmulator()

}
