package com.idzodev.testing.ab.common.di

import com.idzodev.testing.ab.App
import com.idzodev.testing.ab.common.firebase.FirebaseRemoteConfig
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class
])
interface AppComponent {

    val firebaseRemoteConfig: FirebaseRemoteConfig
}



val Any.injector: AppComponent get() = App.appComponent
