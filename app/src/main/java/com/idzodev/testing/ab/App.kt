package com.idzodev.testing.ab

import android.app.Application
import com.idzodev.testing.ab.common.di.AppComponent
import com.idzodev.testing.ab.common.di.AppModule
import com.idzodev.testing.ab.common.di.DaggerAppComponent

class App: Application() {


    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

}
