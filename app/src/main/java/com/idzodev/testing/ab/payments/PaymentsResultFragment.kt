package com.idzodev.testing.ab.payments

import com.idzodev.testing.ab.common.base.ABFragment
import com.idzodev.testing.ab.common.base.AbViewFactory

/**
 * Ab fragment for payment result view
 */
class PaymentsResultFragment: ABFragment() {
    override val featureKey = "paymentResult"

    override fun featureValues(): List<AbViewFactory> = listOf(
        OldPaymentResultViewFactory(),
        NewPaymentResultViewFactory()
    )
}
