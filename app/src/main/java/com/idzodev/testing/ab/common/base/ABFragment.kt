package com.idzodev.testing.ab.common.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.idzodev.testing.ab.common.di.injector
import java.lang.RuntimeException

/**
 * Base fragment for all A/B screens. Use this if you need to show different screens depend on A/B value
 */
abstract class ABFragment: Fragment(){
    /**
     * Feature key that will come from remote config (e.g. Firebase)
     */
    abstract val featureKey: String

    /**
     * All available implementation for each value that can contain [featureKey] var
     */
    abstract fun featureValues(): List<AbViewFactory>

    /**
     * Current [AbView]
     */
    private lateinit var abView: AbView

    /**
     * Current [AbViewFactory]
     */
    private val factory by lazy {
        val featureValue = injector.firebaseRemoteConfig.getString(featureKey)
        val viewFactories = featureValues()
        return@lazy viewFactories.firstOrNull { it.featureValue == featureValue }
            ?: viewFactories.firstOrNull()
            ?: throw RuntimeException("No default view setting up")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //create ab view
        abView = factory.create(container!!)
        return abView.containerView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //observe view using LiveData or simple set values
        abView.observeView(this)
    }
}
