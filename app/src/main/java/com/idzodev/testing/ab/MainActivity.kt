package com.idzodev.testing.ab

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.idzodev.testing.ab.payments.PaymentsResultFragment


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (supportFragmentManager.findFragmentById(R.id.fragment_container) == null) {
            if (savedInstanceState != null){
                return
            }

            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, PaymentsResultFragment())
                .commitAllowingStateLoss()
        }

    }
}